import { CountUp } from 'countup.js';
export default ({ app }, inject) => {
    inject("countUp", CountUp);
}