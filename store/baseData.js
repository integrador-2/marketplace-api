//Logos
import logo from '../static/logos/logo-blanco.png';
import logoColor from '../static/logos/logo-color.png';
import logoMuni from '../static/logos/logo-municipalidad.png'
//
import logoColor1 from '../static/logos/logo-mercado-lima-color-1.png';
import logoColor2 from '../static/logos/logo-mercado-lima-color-2.png';
import menuIcon from '../static/img/menu-icon.png';
import logoMobile from '../static/img/logo-mobile.png';
import homeIcon from '../static/img/home-munlima.png';

import baseURL from '../constant/baseURL';
import domain from '../constant/domain';
import entpTerms from '../constant/entpTerms';
import userTerms from '../constant/userTerms';

export const state = () => ({
    companyName: 'Mercado de Lima',
    baseURL: baseURL,
    domain: domain,
    logo: logo,
    logoMuni: logoMuni,
    logoColor: logoColor,
    logoColor1: logoColor1,
    logoColor2: logoColor2,
    logoMobile: logoMobile,
    menuIcon: menuIcon,
    phone: '(01) 632-3126',
    address: 'Av. Garcilaso de la Vega 1348, cuarto piso, sector B, Cercado de Lima',
    email: 'mercadodelima@munlima.gob.pe',
    homeIcon: homeIcon,
    navigation: [
        { icon: homeIcon, redirectTo: '/' },
        { name: 'Productos Alimentarios', redirectTo: '/' },
        { name: 'Gastronomía', redirectTo: '/' },
        { name: 'Artesanía', redirectTo: '/' },
        { name: 'Experiencias Turísticas', redirectTo: '/' },
    ],
    slides: [],
    countStores: 0,
    countProducts: 0,
    mostCategories: [],
    mostBusiness: [],
    categories: [],
    conglomerates: [],
    departments: [],
    paymentMethods: [],
    metrics: [],
    campaigns: [],
    campaignsForEntrepreneurs: [],
    socialItems: [
        //FontAwesome
        { icon: 'fab fa-facebook-f', redirectTo: 'https://www.facebook.com/LimaDesarrolloEconomico' },
        { icon: 'fab fa-instagram', redirectTo: 'https://www.instagram.com/munlima/' },
        { icon: 'fab fa-twitter', redirectTo: 'https://twitter.com/MuniLima' },
        { icon: 'fab fa-youtube', redirectTo: 'https://www.youtube.com/user/webmunlima' },
    ],
    socialShare: [{
            icon: 'fab fa-facebook-f',
            url: `https://www.facebook.com/sharer/sharer.php?u=`,
            type: 'facebook'
        },
        {
            icon: 'fab fa-twitter',
            url: `https://twitter.com/intent/tweet?text=`,
            type: 'twitter'
        },
        {
            icon: 'fab fa-linkedin-in',
            url: `https://www.linkedin.com/shareArticle?mini=true&url=`,
            type: 'linkedin'
        },
        {
            icon: 'fab fab fa-whatsapp',
            url: `https://api.whatsapp.com/send?text=`,
            type: 'whatsapp'
        }
    ],
    //Terms &  conditions
    entpTerms: entpTerms,
    userTerms: userTerms,
    anounces: [],
    modalAnounces: false,
    featuredProductsCampaign: [],
    currentToken: null
})

export const mutations = {
    setConglomerates(state, conglomerates) {
        state.conglomerates = conglomerates;
    },
    setCountStores(state, countStores) {
        state.countStores = countStores;
    },
    setCountProducts(state, countProducts) {
        state.countProducts = countProducts
    },
    setMostBusiness(state, mostBusiness) {
        state.mostBusiness = mostBusiness;
    },
    setMostCategories(state, mostCategories) {
        state.mostCategories = mostCategories;
    },
    setCategories(state, categories) {
        state.categories = categories;
    },
    setSlides(state, slides) {
        state.slides = slides;
    },
    setDepartments(state, departments) {
        state.departments = departments;
    },
    setMetrics(state, metrics) {
        state.metrics = metrics
    },
    setPaymentMethods(state, paymentMethods) {
        state.paymentMethods = paymentMethods
    },
    setCampaigns(state, campaigns) {
        state.campaigns = campaigns
    },
    setCampaignsForEntrepreneurs(state, campaignsForEntrepreneurs) {
        state.campaignsForEntrepreneurs = campaignsForEntrepreneurs
    },

    setAnounces(state, anounces) {
        state.anounces = anounces;
    },
    setStatusModalAnounces(state, bool) {
        state.modalAnounces = bool;
    },
    setFeaturedProductCampaing(state, featuredProductCampaing) {
        state.featuredProductsCampaign = featuredProductCampaing;
    },
    setCurrentToken(state, token) {
        state.currentToken = token;
    }
}

export const actions = {
    async getCategories({ state, commit }) {
        const res = await this.$axios.get(`/api/categories`);
        if (res.status != 200) return;
        res.data.forEach(cat => {
            cat.subcategories = [{
                name: `Todo ${cat.name}`,
                sku: '',
                _id: `todo-${cat.name}`,
            }, ...cat.subcategories]
        });
        commit('setCategories', res.data)
    },
    async getDepartments({ state, commit }) {
        const res = await this.$axios.get(`/api/departments`);
        if (res.status != 200) return;
        commit('setDepartments', res.data);
    },
    async getProvincesByDepartmentId({ state, commit }, departmentId) {
        const res = await this.$axios.get(`/api/provinces/departments/${departmentId}`);
        if (res.status != 200) return;
        return res.data;
    },
    async getDistrictsByProvinceId({ state, commit }, provinceId) {
        const res = await this.$axios.get(`/api/districts/provinces/${provinceId}`);
        if (res.status != 200) return;
        return res.data;
    },
    async registerStore({ state }, storeData) {
        const res = await this.$axios.post(`/api/requests/stores/create`, storeData);
        return res;
    },
    async getMetrics({ state, commit }) {
        const res = await this.$axios.get('/api/metrics');
        if (res.status != 200) return;
        commit('setMetrics', res.data);
    },
    async getPaymentMethods({ state, commit }) {
        const res = await this.$axios.get('/api/payment-methods');
        if (res.status != 200) return;
        commit('setPaymentMethods', res.data);
    },
    async getConglomerates({ state, commit }) {
        if (state.conglomerates && state.conglomerates.length > 0) return state.conglomerates;
        const res = await this.$axios.get("/api/emporius");
        if (res.status != 200) return [];
        commit("setConglomerates", res.data);
        return res.data;
    },
    async getCampaigns({ state, commit }) {
        const res = await this.$axios.get('/api/campaigns')
        if (res.status != 200) return [];
        commit("setCampaigns", res.data);
        return res.data;
    },
    async getCampaignsForEntrepreneurs({ state, commit }) {
        const res = await this.$axios.get('/api/campaigns/entrepreneur')
        if (res.status != 200) return [];
        commit("setCampaignsForEntrepreneurs", res.data);
        return res.data;
    },
    async getFeaturedProductsFromCampaigns({ state, commit }) {
        const res = await this.$axios.get('/api/campaigns/featured_products')
        if (res.status != 200) return [];
        commit("setFeaturedProductCampaing", res.data);
        return res.data;
    },
    async getAnounces({ state, commit, dispatch }) {
        const { status, data } = await this.$axios.get(`/api/pop_ups/visible`);
        if (status != 200) return null;
        commit('setAnounces', data);
        return data;
    },
    async refreshToken({ state, commit }) {
        const { data, status } = await this.$axios.post("/api/auth/refresh_token");
        if (status != 200 && status != 201 && status != 204) {
            this.$auth.logout();
            localStorage.setItem("iat", false);
            return;
        }
        commit("setCurrentToken", `Bearer ${data.access_token}`);
        localStorage.setItem("iat", (new Date().getTime() + 1.56e6));
        localStorage.setItem("auth._token.local", state.currentToken);
        this.$axios.setHeader("Authorization", state.currentToken);
        this.$auth.strategy.setUserToken(data.access_token);
    },
    openModalAnounces({ state, commit }) {
        commit('setStatusModalAnounces', true);
    },
    closeModalAnounces({ state, commit }) {
        commit('setStatusModalAnounces', false);
    },
}