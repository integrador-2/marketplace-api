import logoML from '../static/logos/logo-color.png';
import baseURL from '../constant/baseURL';

export default (querys) => {
    return {
        title: querys.search ? `${querys.search} en Mercado de Lima` : 'Buscador - Mercado de Lima',
        meta: [{
                hid: "search-name",
                name: "name",
                content: querys.search ? `Encuentra ${querys.search} en Mercado de Lima` : 'Buscador - Mercado de Lima',
            },
            {
                hid: "search-description",
                name: "description",
                content: querys.search ? `Encuentra ${querys.search} en Mercado de Lima` : 'Buscador - Mercado de Lima',
            },
            {
                hid: "search-keyword",
                name: "keyword",
                content: querys.search ? `Encuentra ${querys.search} en Mercado de Lima` : 'Buscador - Mercado de Lima',
            },
            {
                hid: "logo-marketplace",
                name: "msapplication-TileImage",
                content: baseURL + logoML,
            },
        ]
    }
}