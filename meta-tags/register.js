import logoML from '../static/logos/logo-color.png';
import imgEcommerce from '../static/bg/ecommerce.jpg';
import baseURL from '../constant/baseURL';

export default () => {
    return {
        title: `Registrate en Mercado de Lima`,
        meta: [{
                hid: "field-name",
                name: "name",
                content: `Registrate en Mercado de Lima`,
            },
            {
                hid: "field-description",
                name: "description",
                content: `Registrate en Mercado de Lima para comenzar a vender tus productos`,
            },
            {
                hid: "field-keyword",
                name: "keyword",
                content: 'Registro, Mercado de lima, Marketplace de Lima, Comprale a Lima'
            },
            {
                hid: "logo-marketplace",
                name: "msapplication-TileImage",
                content: baseURL + logoML,
            },
            //OG ITEMS
            {
                hid: 'og-title',
                property: 'og:title',
                content: 'Registrate en Mercado de Lima',
            },
            {
                hid: 'og-type',
                property: 'og:type',
                content: 'article'
            },
            {
                hid: "og-image",
                property: "og:image",
                content: baseURL + imgEcommerce,
            },
            {
                hid: "og-description",
                property: "og:description",
                content: 'Encuentra lo que buscas. Compra y vende productos con Mercado de Lima',
            },
            {
                hid: "og-site-name",
                property: "og:site_name",
                content: 'Mercado de Lima'
            },
            {
                hid: "og-url",
                property: "og:url",
                content: baseURL + "/auth/registro",
            },
            //Twitter items
            {
                hid: 'twitter-title',
                property: 'twitter:title',
                content: `Registrate en Mercado de Lima`,
            },
            {
                hid: "twitter-image",
                property: "twitter:image",
                content: baseURL + imgEcommerce,
            },
            {
                hid: "twitter-description",
                property: "twitter:description",
                content: 'Encuentra lo que buscas. Compra y vende productos con Mercado de Lima',
            },
            {
                hid: "twitter-site",
                property: "twitter:site",
                content: 'Mercado de Lima'
            },
            {
                hid: "twitter-creator",
                property: "twitter:creator",
                content: 'Mercado de Lima'
            },
        ]
    }
}