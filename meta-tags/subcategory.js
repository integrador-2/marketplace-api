import logoML from '../static/logos/logo-color.png';
import baseURL from '../constant/baseURL';

export default (category, params) => {
    const subcat = category.subcategories ? category.subcategories.find(s => s.sku === params.skuSubCategory) : null;
    if (!subcat) return {
        title: "Subcategoría no encontrada"
    }
    return {
        title: category.name ? `${category.name} ${subcat ? ' - '+subcat.name : ''} en Mercado de Lima` : `Mercado de Lima`,
        meta: [{
                hid: "category-name",
                name: "name",
                content: category.name ? `${category.name} ${subcat ? ' - '+subcat.name : ''} en Mercado de Lima` : `Mercado de Lima`,
            },
            {
                hid: "category-description",
                name: "description",
                content: category.name ? `${category.name} ${subcat ? ' - '+subcat.name : ''} en Mercado de Lima` : `Mercado de Lima`,
            },
            {
                hid: "category-keyword",
                name: "keyword",
                content: category.name ? `${category.name} ${subcat ? ' - '+subcat.name : ''} en Mercado de Lima` : `Mercado de Lima`,
            },
            {
                hid: "logo-marketplace",
                name: "msapplication-TileImage",
                content: logoML,
            },
            //OG ITEMS
            {
                hid: 'og-title',
                property: 'og:title',
                content: category.name ? `${category.name} ${subcat ? ' - '+subcat.name : ''} en Mercado de Lima` : `Mercado de Lima`,
            },
            {
                hid: 'og-type',
                property: 'og:type',
                content: 'article'
            },
            {
                hid: "og-image",
                property: "og:image",
                content: category.image ? `${baseURL}/file/${category.image.filename }` : baseURL + logoML,
            },
            {
                hid: "og-description",
                property: "og:description",
                content: category.name ? `${category.name} ${subcat ? ' - '+subcat.name : ''} en Mercado de Lima` : `Mercado de Lima`,
            },
            {
                hid: "og-site-name",
                property: "og:site_name",
                content: 'Mercado de Lima'
            },
            {
                hid: "og-url",
                property: "og:url",
                content: `${baseURL}/categorias/${category.sku}/${subcat.sku}`,
            },
            //Twitter items
            {
                hid: 'twitter-title',
                property: 'twitter:title',
                content: category.name ? `${category.name} ${subcat ? ' - '+subcat.name : ''} en Mercado de Lima` : `Mercado de Lima`,
            },
            {
                hid: "twitter-image",
                property: "twitter:image",
                content: category.image ? `${baseURL}/file/${category.image.filename }` : baseURL + logoML,
            },
            {
                hid: "twitter-description",
                property: "twitter:description",
                content: category.name ? `${category.name} ${subcat ? ' - '+subcat.name : ''} en Mercado de Lima` : `Mercado de Lima`,
            },
            {
                hid: "twitter-site",
                property: "twitter:site",
                content: 'Mercado de Lima'
            },
            {
                hid: "twitter-creator",
                property: "twitter:creator",
                content: 'Mercado de Lima'
            },
        ]
    }
}