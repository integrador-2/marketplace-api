import logoML from '../static/logos/logo-color.png';
import baseURL from '../constant/baseURL';

export default (campaign) => {
    if (!campaign) return {
        title: 'Campaña no disponible',
    }
    return {
        title: campaign && campaign.title ? `${campaign.title}` : `Campaña de Mercado de Lima`,
        meta: [{
                hid: "campaign-title",
                name: "name",
                content: campaign && campaign.title ? `${campaign.title}` : `Campaña de Mercado de Lima`
            },
            {
                hid: "campaign-subtitle",
                name: "description",
                content: campaign && campaign.subtitle ? campaign.subtitle : campaign.subtitle,
            },
            {
                hid: "campaign-keyword",
                name: "keyword",
                content: campaign && campaign.title ? `${campaign.title}` : 'campaña',
            },
            {
                hid: "logo-campaign",
                name: "msapplication-TileImage",
                content: baseURL + logoML,
            },
            //OG ITEMS
            {
                hid: 'og-title',
                property: 'og:title',
                content: campaign && campaign.title ? `${campaign.title}` : `Campaña de Mercado de Lima`
            },
            {
                hid: 'og-type',
                property: 'og:type',
                content: 'article'
            },
            {
                hid: "og-image",
                property: "og:image",
                content: campaign && campaign.image ? `${baseURL}/file/${campaign.image.filename }` : baseURL + logoML,
            },
            {
                hid: "og-description",
                property: "og:description",
                content: campaign && campaign.subtitle ? campaign.subtitle : '',
            },
            {
                hid: "og-site-name",
                property: "og:site_name",
                content: 'Mercado de Lima'
            },
            {
                hid: "og-url",
                property: "og:url",
                content: `${baseURL}/${campaign.sku}`,
            },
            //Twitter items
            {
                hid: 'twitter-title',
                property: 'twitter:title',
                content: campaign && campaign.title ? `${campaign.title}` : `Campaña de Mercado de Lima`
            },
            {
                hid: "twitter-image",
                property: "twitter:image",
                content: campaign && campaign.image ? `${baseURL}/file/${campaign.image.filename }` : baseURL + logoML,
            },
            {
                hid: "twitter-description",
                property: "twitter:description",
                content: campaign && campaign.subtitle ? campaign.subtitle : '',
            },
            {
                hid: "twitter-site",
                property: "twitter:site",
                content: 'Mercado de Lima'
            },
            {
                hid: "twitter-creator",
                property: "twitter:creator",
                content: 'Mercado de Lima'
            },
        ]
    }
}