import logoML from '../static/logos/logo-color.png';
import imgEcommerce from '../static/bg/ecommerce.jpg';
import baseURL from '../constant/baseURL';

export default () => {
    return {
        title: `Iniciar sesión en Mercado de Lima`,
        meta: [{
                hid: "field-name",
                name: "name",
                content: `Iniciar sesión en Mercado de Lima`,
            },
            {
                hid: "field-description",
                name: "description",
                content: `Ingresa para administrar tus productos`,
            },
            {
                hid: "field-keyword",
                name: "keyword",
                content: 'Inicio de sesión, Mercado de lima, Login, Comprale a Lima, web lima'
            },
            {
                hid: "logo-marketplace",
                name: "msapplication-TileImage",
                content: baseURL + logoML,
            },
            //OG ITEMS
            {
                hid: 'og-title',
                property: 'og:title',
                content: 'Inicia sesión en Mercado de Lima',
            },
            {
                hid: 'og-type',
                property: 'og:type',
                content: 'article'
            },
            {
                hid: "og-image",
                property: "og:image",
                content: baseURL + imgEcommerce,
            },
            {
                hid: "og-description",
                property: "og:description",
                content: 'Inicia sesión en Mercado de Lima y publica tus productos',
            },
            {
                hid: "og-site-name",
                property: "og:site_name",
                content: 'Mercado de Lima'
            },
            {
                hid: "og-url",
                property: "og:url",
                content: baseURL + "/auth/login",
            },
            //Twitter items
            {
                hid: 'twitter-title',
                property: 'twitter:title',
                content: 'Mercado de Lima',
            },
            {
                hid: "twitter-image",
                property: "twitter:image",
                content: baseURL + imgEcommerce,
            },
            {
                hid: "twitter-description",
                property: "twitter:description",
                content: 'Inicia sesión en Mercado de Lima y publica tus productos',
            },
            {
                hid: "twitter-site",
                property: "twitter:site",
                content: 'Mercado de Lima'
            },
            {
                hid: "twitter-creator",
                property: "twitter:creator",
                content: 'Mercado de Lima'
            },
        ]
    }
}