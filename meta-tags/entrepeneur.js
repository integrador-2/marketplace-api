import logoML from '../static/logos/LOGO-MERCADO-LIMA-CARRITO-FORMAL.png';
import baseURL from '../constant/baseURL';

export default (store) => {
    if (!store) return {
        title: 'Emprendimiento no disponible',
    }
    return {
        title: store && store.businessName ? `${store.businessName} en Mercado de Lima` : `Emprendimientos en Mercado de Lima`,
        meta: [{
                hid: "business-name",
                name: "name",
                content: store && store.businessName ? `${store.businessName} en Mercado de Lima` : `Emprendimientos en Mercado de Lima`
            },
            {
                hid: "business-description",
                name: "description",
                content: store && store.storeReview ? store.storeReview : store.productsResume,
            },
            {
                hid: "business-keyword",
                name: "keyword",
                content: store && store.businessName ? `${store.businessName}` : '',
            },
            {
                hid: "logo-marketplace",
                name: "msapplication-TileImage",
                content: baseURL + logoML,
            },
            //OG ITEMS
            {
                hid: 'og-title',
                property: 'og:title',
                content: store && store.businessName ? `${store.businessName} en Mercado de Lima` : `Emprendimientos en Mercado de Lima`,
            },
            {
                hid: 'og-type',
                property: 'og:type',
                content: 'article'
            },
            {
                hid: "og-image",
                property: "og:image",
                content: store && store.images && store.images[0] ? `${baseURL}/file/${store.images[0].filename }` : baseURL + logoML,
            },
            {
                hid: "og-description",
                property: "og:description",
                content: store && store.storeReview ? store.storeReview : store.productsResume,
            },
            {
                hid: "og-site-name",
                property: "og:site_name",
                content: 'Mercado de Lima'
            },
            {
                hid: "og-url",
                property: "og:url",
                content: `${baseURL}/${store.sku}`,
            },
            //Twitter items
            {
                hid: 'twitter-title',
                property: 'twitter:title',
                content: store && store.businessName ? `${store.businessName} en Mercado de Lima` : `Emprendimientos en Mercado de Lima`,
            },
            {
                hid: "twitter-image",
                property: "twitter:image",
                content: store && store.images && store.images[0] ? `${baseURL}/file/${store.images[0].filename }` : baseURL + logoML,
            },
            {
                hid: "twitter-description",
                property: "twitter:description",
                content: store && store.storeReview ? store.storeReview : store.productsResume,
            },
            {
                hid: "twitter-site",
                property: "twitter:site",
                content: 'Mercado de Lima'
            },
            {
                hid: "twitter-creator",
                property: "twitter:creator",
                content: 'Mercado de Lima'
            },
        ]
    }
}