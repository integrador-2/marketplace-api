import logoML from '../static/logos/logo-color.png';
import imgEcommerce from '../static/bg/og-image.jpg';
import baseURL from '../constant/baseURL';

export default () => {
    return {
        meta: [
            { hid: 'description', name: 'description', content: 'Encuentra lo que buscas. Compra y vende productos en Mercado de Lima', },
            { hid: 'keyword', name: 'keyword', content: 'Mercado, Mercado de lima, lima, productos, venta, compras, encuentra lo que buscas, busca, servicios, LIMA, marketplace, web lima' },
            //OG ITEMS
            {
                hid: 'og-title',
                property: 'og:title',
                content: 'Mercado de Lima',
            },
            {
                hid: 'og-type',
                property: 'og:type',
                content: 'website'
            },
            {
                hid: "og-image",
                property: "og:image",
                content: baseURL + imgEcommerce,
            },
            {
                hid: "og-description",
                property: "og:description",
                content: 'Encuentra lo que buscas. Compra y vende productos con Mercado de Lima',
            },
            {
                hid: "og-site-name",
                property: "og:site_name",
                content: 'Mercado de Lima'
            },
            {
                hid: "og-url",
                property: "og:url",
                content: baseURL,
            },
            //Twitter items
            {
                hid: 'twitter-title',
                property: 'twitter:title',
                content: 'Mercado de Lima',
            },
            {
                hid: "twitter-image",
                property: "twitter:image",
                content: baseURL + imgEcommerce,
            },
            {
                hid: "twitter-description",
                property: "twitter:description",
                content: 'Encuentra lo que buscas. Compra y vende productos con Mercado de Lima',
            },
            {
                hid: "twitter-site",
                property: "twitter:site",
                content: 'Mercado de Lima'
            },
            {
                hid: "twitter-creator",
                property: "twitter:creator",
                content: 'Mercado de Lima'
            },

        ]
    }
}