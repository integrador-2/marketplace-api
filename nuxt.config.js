import { es } from 'vuetify/es5/locale';
import colors from 'vuetify/es5/util/colors';
import baseURL from './constant/baseURL';

export default {
    // Global page headers (https://go.nuxtjs.dev/config-head)
    head: {
        //titleTemplate: '',
        /* titleTemplate: 'Mercado de Lima', */
        title: 'Mercado de Lima',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Encuentra lo que buscas. Compra y vende productos en Mercado de Lima', },
            { hid: 'keyword', name: 'keyword', content: 'Mercado, Mercado de lima, lima, productos, venta, compras, encuentra lo que buscas, busca, servicios, LIMA, marketplace, web lima' },
            { hid: 'og-locale', name: 'og-locale', content: 'es_PE' },
        ],
        link: [
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;500;600;700;800;900&display=swap' },
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap' },
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Lato:wght@300;400;500;600;700&display=swap' },
            /* { rel: 'stylesheet', type: 'text/css', href: 'https://unpkg.com/swiper/swiper-bundle.min.css' }, */
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons' },
            { rel: 'icon', type: 'image/x-icon', href: '/logos/favicon-32x32-blanco.png' },
            { rel: 'stylesheet', type: 'text/css', href: 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css' },
        ],
        script: [
            { type: "text/javascript", src: "https://kit.fontawesome.com/73a9e59589.js" },
            /* {
                src: "https://unpkg.com/swiper/swiper-bundle.min.js",
                type: "text/javascript"
            }, */
        ],
    },
    // loader progress
    loading: {
        color: '#053273',
        failedColor: "#af0000",
        height: '2px'
    },

    // Global CSS (https://go.nuxtjs.dev/config-css)
    css: [
        '~/assets/colors.css',
        '~/assets/main.css',
    ],

    // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
    plugins: [
        { src: '~/plugins/axios', ssr: false },
        { src: '~/plugins/vueMask', ssr: false },
        { src: '~/plugins/vueLazy', ssr: false },
        { src: '~/plugins/countup', mode: 'client' },
        { src: '~/plugins/swiper', mode: 'client' },
        { src: '~/plugins/vue-apexchart.js', ssr: false }
    ],

    // Auto import components (https://go.nuxtjs.dev/config-components)
    components: true,

    // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
    buildModules: [
        // https://go.nuxtjs.dev/vuetify
        '@nuxtjs/vuetify',
        '@nuxtjs/google-analytics',
    ],

    // Modules (https://go.nuxtjs.dev/config-modules)
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        //'@nuxtjs/auth'
        '@nuxtjs/auth'
    ],

    // Axios module configuration (https://go.nuxtjs.dev/config-axios)
    axios: {
        baseURL: baseURL,
        headers: {
            'Content-Type': 'application/json'
        }
    },

    // Nuxt Google Analytics
    googleAnalytics: {
        // Options
        id: 'UA-71354492-4',
        autoTracking: {
            screenview: true
        }
    },
    // Nuxt Auth
    auth: {
        strategies: {
            local: {
                token: {
                    property: 'access_token',
                    maxAge: 1800,
                    type: 'Bearer',
                    required: true,
                },
                user: {
                    property: 'user',
                    autoFetch: true
                },
                endpoints: {
                    login: { url: '/api/auth/signin', method: 'post', propertyName: 'access_token' },
                    user: { url: '/api/auth/profile', method: 'get', propertyName: 'user' },
                    logout: false,
                },
                tokenRequired: true,
                autoLogout: true,
                tokenType: 'Bearer'
            }
        }
    },

    // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
    vuetify: {
        customVariables: ['~/assets/variables.scss'],
        lang: {
            locales: { es },
            current: "es",
        },
        theme: {
            //dark: true,
            themes: {
                light: {
                    primary: "#00A39C", //Turquesa
                    secondary: "#FCA700", //Naranja
                    blue: "#1A4A84", //Azul
                    tertiary: "#1A4A84", //Azul
                    black: "#000", // Negro
                    white: '#fff', //blanco
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3,
                },
                dark: {
                    primary: '#fff',
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    white: '#fff', //blanco
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3,
                },
            }
        }
    },

    // Build Configuration (https://go.nuxtjs.dev/config-build)
    build: {

    }
}